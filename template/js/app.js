var app = angular.module('app', []);


app.filter('parentFilter', function() {
  return function(list, parent, property, emptyProperty) {
    console.log(list);
    console.log(parent);
    console.log(property);
    console.log(emptyProperty);
    // return list;
    return list.reduce(function(filteredList, child) {
      if(child._source.hasOwnProperty(property) && child._source[property] == parent._id) {
        // console.log(emptyProperty);
        if(emptyProperty == undefined || !child._source.hasOwnProperty(emptyProperty) || child._source[emptyProperty] == null) {
          filteredList.push(child);
        }
      }
      return filteredList;
    }, []);
  }
});

app.service('ElasticSearchServices', function($http) {

  this.getRegistrationRequests = function(callback) {
    // $http.get('/registration/requests').then(callback);
  }

  this.getTagGrpList = function(callback) {
    $http.get('ajax.php?action=getTagGrpList').then(callback);
  }

  this.getTagTypeList = function(callback) {
    $http.get('ajax.php?action=getTagTypeList').then(callback);
  }

  this.getTagsList = function(callback) {
    $http.get('ajax.php?action=getTagsList').then(callback);
  }

});


app.controller('SearchController', function($scope, ElasticSearchServices) {

  $scope.tagGrpList = [];
  $scope.tagTypeList = [];
  $scope.tagList = [];
  $scope.query = {

  };

  var init = function() {

    ElasticSearchServices.getTagGrpList(function(res) {
      console.log(res);
      $scope.tagGrpList = res.data.hits.hits;
    });

    ElasticSearchServices.getTagTypeList(function(res) {
      console.log(res);
      $scope.tagTypeList = res.data.hits.hits;
    });

    ElasticSearchServices.getTagsList(function(res) {
      console.log(res);
      $scope.tagList = res.data.hits.hits;
    });

    $scope.query = {
      'sort': "",
      'tags': []      
    };
  }

  init();

  $scope.filterTagTypes = function(group) {
    return !($scope.assocCompanies.reduce(function(prev, iComp) {
      return (prev | (iComp.id == company.id));
    }, false));
  }

  $scope.checkTag = function(tag) {
    if(tag.isChecked) {
      $scope.query.tags.push(tag);
    } else {
      $scope.query.tags.splice($scope.query.tags.indexOf(tag), 1);
    }

  }

  $scope.removeTagFromQuery = function(tag) {
    $scope.query.tags.splice($scope.query.tags.indexOf(tag), 1);
  }

});